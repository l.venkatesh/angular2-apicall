import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css', './bootstrap-responsive.min.css', './font-awesome.css', './bootstrap.min.css']
})
export class AppComponent {
  title = 'app';
}
