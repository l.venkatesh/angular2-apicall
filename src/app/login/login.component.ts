import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {Headers} from '@angular/http';
import {LoginService} from './login.service';
import {Observable} from 'rxjs/Rx';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css', '../bootstrap.min.css', '../bootstrap-responsive.min.css', '../font-awesome.css']
})

export class LoginComponent implements OnInit {
  username: string;
  password: string;

  constructor(private router : Router, private _loginService : LoginService) { }

  ngOnInit() {
  }

  login() {        
    let userCredentials = {"user_name": this.username, "password": this.password};
    this._loginService.userAuthentication(userCredentials)
      .subscribe(res => { 

        console.log('user_id: ' + res['user_id']); 
        console.log('user_role_id: ' + res['user_role_id']); 
        console.log('first_name: ' + res['first_name']); 
        console.log('last_name: ' + res['last_name']); 
        console.log('auth_token: ' + res['auth_token']); 

        this.router.navigate(['dashboard']); 
        return true; 
      }, 
      error => { 
        alert("Invalid credentials.");
        console.log(error); return Observable.throw(error); 
      });   
  }

}
