import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class LoginService {

  constructor(private http:HttpClient) { }

  userAuthentication(userCredentials) {
        let body = JSON.stringify(userCredentials);
        return this.http.post('https://aquamesh-test.mcwane.co.in/1.0/users/authentication', body);
  }
}